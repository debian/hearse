# $Id: hearse.cron.d,v 1.3 2002-06-04 03:10:02 roderick Exp $
#
# The standard Debian and RPM installations run hearse from /etc/cron.daily.
# This allows it to work (via anacron) with machines which aren't left on
# all the time.
#
# hearse itself works well if run frequently.  If it doesn't find any
# new bones files it doesn't even contact the server.
#
# To run it more often than once a day, you can uncomment the cron entry
# here.  You should also make a change at the top of /etc/cron.daily/hearse
# as indicated by the comments there, else you'll get the occasional email
# as both try to run at once and the locking kicks in.

# If you'd like to see what the server's doing, you can use --cron rather
# than --quiet.  This will cause it to output its status messages, but
# only when it actually transfers a bones file.

#*/15 * * * * root test -f /usr/bin/hearse -a -s /etc/nethack/hearse.user-token && hearse --quiet
