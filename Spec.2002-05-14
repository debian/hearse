$Id: Spec.2002-05-14,v 1.1 2005-10-06 10:52:43 roderick Exp $

Message-ID: <014601c1fb74$755caaf0$8b0224d9@bingalybong>
From: "Alexis Manning" <alexismanning@bigfoot.com>
To: "Roderick Schertler" <roderick@argon.org>
References: <b37306f8.0205071935.78c8056d@posting.google.com> <CkhC8.38769$26.3160662@typhoon.maine.rr.com> <abc8pp$hbr4t$1@ID-105700.news.dfncis.de> <pzhelaob00.fsf@eeyore.ibcinc.com>
Subject: Re: Is Hearse safe to use with 3.4.0?
Date: Tue, 14 May 2002 19:23:34 +0100

From: "Roderick Schertler" <roderick@argon.org>
> At the risk of invoking the Curse of Hearse, I'm interested in writing a
> Unix version, particularly if it can use your server.  Would you send me
> what info I'd need to interoperate with the server, or the source for
> the client?  Thanks very much.

A brave man!

Well, someone else has offered and is currently (hopefully) perusing the
source code with an eye to get a-porting.  So we have various options:
    * You could offer to work on it with him
    * You could wait and see if he flakes / if the Curse of Hearse zaps him
    * You could work on it separately  (Although I will of course let him
know if you want to do that.  Not fair to keep people in the dark)

I am blissfully indifferent, I don't care particularly much how it's done as
long as it's done.  So let me know what you'd prefer.

In the meantime, here is some light music.

Sorry, no.  In the meantime, here is some stuff I've worked about about
Windows/Unix differences for Hearse which you may find interesting, or may
scare you off the task altogether.

Cheers,

-- A.
====

How Hearse works:
--------------------

The Hearse client is simple.  A couple of bits of data are stored in the
registry, including the date the user last uploaded bones, and a per-user
token.  If the user hasn't used Hearse before, he is prompted for his email
address and the server offers him a per-user token in exchange.  This is
stuffed into the registry and sent with all subsequent requests, and is used
to track who uploaded what bones file (so no one gets the same bones file
twice), and used to selectively block abusive users of the system.  The
per-user token is 32 characters of gibberish, if you care.

After that's out of the way, the process is dirt simple:
    Loop over all bones files that are newer than the last bones date
        Generate an MD5 hash for that file
        Ask the server if it's OK
        If the server says "Yup", submit the bones file
   End loop
   If any bones files were submitted
     Loop
       Ask the server for a bones file
     Repeat until it says No
     Update the last bones date

That's it.  It does get a leetle more complicated, but not much.  A list of
all bones files in that directory is generated and sent to the server during
the download phase so you're not offered a bones file that you already have.
All the communication is done by hitting variations of a fixed URL and
passing arguments as HTTP headers.  Any errors (e.g. "No more bones for
you") are handled by the server passing back a header that indicates that
the body is an error message that should be displayed.

Headers are:
X_USERTOKEN:  per user token
X_HEARSECRC: MD5 hash of the Hearse executable, used to determine whether a
newer version is available.  We'll work out something appropriate to be sent
for Unix.  Sending 'noupdate' will prevent any update being offered.
X_BONESCRC: MD5 hash of the current bones file
X_VER1           Ugly and non-extensible way of sending the bonesfile "magic
number" information.
X_VER2           Done this way to allow for the possibility of masking the
parts of the version
X_VER3           struct that don't really matter at some point in the dim
and distant.
X_VER4
X_ERROR        Was there an error?  If present, is the body some text that
should be displayed (INFO), or should the body be displayed then the client
terminate?  (FATAL)
X_FILENAME  Name of the bonesfile in the body of the message
X_USERLEVELS    Comma delimited listed of the bonesfiles that the user
already has -- don't offer them bonD0.5 if they already have a bonD0.5
X_GIVEINFO    A crappily named header.  Not currently used, but set with the
uploaded first bones file of that session.  The idea is that it could be
used to trigger a Message of the Day or some such.
X_CLIENTID    Always set to PC-HEARSE, unused.  For future expansion.  Yours
could say "UNIX-HEARSE", so if different server behaviour was needed for the
two clients, it could be easily coded.
X_FORCEUPDATE    Server demands that you update to the latest version to use
Hearse.  Windows versions auto-update.


Hearse and Unix
-----------------

The *'s indicate current thinking on the subject, not set in stone at all.
Things in square brackets are really server-side issues that I've just put
in to remind me.  Let me know any particular part makes no sense, or if it
sounds like I've missed something.


Hearse stores a per-user token which is sent with every request to the
server.  This is so that people don't download the same bones file more than
once.  It's also used to track users in case of abuse.  Should this be
per-site on Unix rather than per-user?
 * Per-site is probably better.  The only user who can create a wizard bones
file under Unix is root, so it's likely to be safe.  Worst case, whole site
has to be banned.


File ownership issues
 * Hearse will likely have to run setuid nethack so it can access bones
files.


Hearse has AutoUpdate functionality.  Best way to handle this under Unix?
 * Simplest would probably be to display the error message returned from the
server and quit.  They'll have to download the latest source.
 [Also worth having the server email all the users when an incompatible
version of Hearse is released]


Waaah!  Byte ordering
 * Unlikely to be a problem since the magic numbers will also have their
byte order buggered.  Just make sure the version info is read in byte by
byte, not DWORD by DWORD :)


Hearse stores various bits of data in the registry.  What to do?
 * Have a /etc/hearse.conf file that stores:
  - location of nethack bones
  - Hearse server address [note: this is hardcoded into the client for the
Windows version.  Ewww]
  - File access/owner modes to set on downloaded bones
  - Date of last bones upload
 * Possible race conditions on this file if multiple users run Hearse
simultaneously?
